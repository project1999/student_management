<?php

namespace Models;

use DatabaseConnection\DatabaseConnection;
use Course;

require_once('../Entity/Course.php');
require_once('../Model/TeacherModel.php');
require_once('../DatabaseConnection/DatabaseConnection.php');

/**
 * Class CourseModel
 * @package Models
 */
class CourseModel
{
    /**
     * @var \mysqli
     */
    private \mysqli $connection;

    /**
     * @var Course
     */
    private Course $course;

    /**
     * CourseModel constructor.
     */
    public function __construct()
    {
        $database = new DatabaseConnection();
        $this->connection = $database->database;
    }

    /**
     * @param $courseName
     * @return Course
     */
    public function createCourse($courseName)
    {
        $course = new Course();
        $query = "SELECT * FROM courses WHERE name='$courseName'";
        $result = $this->connection->query($query);
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $course->setId($row['Id']);
                $course->setName($row['Name']);
                $course->setTeacherId($row['Teacher_id']);
                $course->setLaboratory($row['Laboratory']);
                $course->setSeminar($row['Seminar']);
                $course->setCredits($row['Credits']);
                $course->setMaterial($row['Material']);
            }
            return $course;
        }
    }

    /**
     * @param $courseName
     * @return Course
     */
    public function createCourseById($courseId)
    {
        $course = new Course();
        $query = "SELECT * FROM courses WHERE id=$courseId";
        $result = $this->connection->query($query);
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $course->setId($row['Id']);
                $course->setName($row['Name']);
                $course->setTeacherId($row['Teacher_id']);
                $course->setLaboratory($row['Laboratory']);
                $course->setSeminar($row['Seminar']);
                $course->setCredits($row['Credits']);
                $course->setMaterial($row['Material']);
            }
            return $course;
        }
    }

    /**
     * @param $teacherId
     * @return mixed
     */
    public function getCoursesByTeacherId($teacherId)
    {
        $query = "SELECT * FROM courses WHERE teacher_id='$teacherId'";
        $courses[] = new Course();
        $iterator = 0;
        $result = $this->connection->query($query);
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $course = new Course();
                $course->setId($row['Id']);
                $course->setName($row['Name']);
                $course->setLaboratory($row['Laboratory']);
                $course->setSeminar($row['Seminar']);
                $course->setCredits($row['Credits']);
                $course->setMaterial($row['Material']);
                $course->setTeacherId($teacherId);
                $courses[$iterator] =  $course;
                $iterator++;
            }

            return $courses;
        }
    }

    /**
     * @return mixed
     */
    public function getCourses()
    {
        $courses[] = new Course();
        $query = "SELECT name FROM courses";
        $result = $this->connection->query($query);
        $names = $result->fetch_all();
        $iterator = 0;
        foreach ($names as $name) {
            $courses[$iterator] = $this->createCourse($name[0]);
            $iterator++;
        }
        return $courses;
    }

    /**
     * @return bool
     */
    public function addCourse()
    {
        $teacherModel = new TeacherModel();
        if (isset($_POST['submit'])) {
            $name = $_POST['name'];
            if ($_POST['laboratory']) {
                $laboratory = true;
            } else {
                $laboratory = false;
            }
            if ($_POST['seminar']) {
                $seminar = true;
            } else {
                $seminar = false;
            }
            $teacher = $teacherModel->getTeacherId($_SESSION['username']);
            $credits = $_POST['credits'];
            $material = $_POST['material'];
            $query = "INSERT INTO courses (Name, Teacher_id, Laboratory, Seminar, Credits, Material)
	            VALUES ('$name','$teacher[0]','$laboratory','$seminar','$credits','$material')";
            mysqli_query($this->connection, $query);
            return true;
        }
        return false;
    }

    /**
     * @return bool
     */
    public function editCourse()
    {
        if (isset($_POST['save'])) {
            $name = $_POST['name'];
            if ($_POST['laboratory']) {
                $laboratory = true;
            } else {
                $laboratory = false;
            }
            if ($_POST['seminar']) {
                $seminar = true;
            } else {
                $seminar = false;
            }
            $credits = $_POST['credits'];
            $material = $_POST['material'];
            $query = "UPDATE courses SET Laboratory='$laboratory', 
                    Seminar='$seminar', Credits=$credits, Material='$material' WHERE Name='$name'";

            mysqli_query($this->connection, $query);
            return true;
        }
        return false;
    }

    /**
     * @return bool
     */
    public function deleteCourse()
    {
        if (isset($_POST['delete'])) {
            $name = $_POST['deleteName'];
            $query = "DELETE FROM courses WHERE Name='$name'";
            mysqli_query($this->connection, $query);
            return true;
        }
        return false;
    }


}