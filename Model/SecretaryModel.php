<?php

namespace Models;

use DatabaseConnection\DatabaseConnection;
use Secretary;
require_once('../Entity/Secretary.php');

/**
 * Class SecretaryModel
 * @package Models
 */
class SecretaryModel
{
    /**
     * @var \mysqli
     */
    private \mysqli $connection;

    /**
     * @var Secretary
     */
    private Secretary $secretary;

    /**
     * StudentModel constructor.
     */
    public function __construct()
    {
        $database = new DatabaseConnection();
        $this->connection = $database->database;
        $this->secretary = new Secretary();
    }

    /**
     * @param $username
     * @return Secretary
     */
    public function createSecretary($username)
    {
        $query = "SELECT * FROM secretaries WHERE username='$username'";
        $result = $this->connection->query($query);
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $this->secretary->setId($row['Id']);
                $this->secretary->setFirstname($row['Firstname']);
                $this->secretary->setLastname($row['Lastname']);
                $this->secretary->setCnp($row['CNP']);
                $this->secretary->setPhone($row['Phone']);
                $this->secretary->setBirthday($row['Birthday']);
                $this->secretary->setAddress($row['Address']);
                $this->secretary->setUsername($row["Username"]);
                $this->secretary->setUserRole($row['User_role']);
            }
            return $this->secretary;
        }
    }
}