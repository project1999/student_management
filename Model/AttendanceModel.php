<?php


namespace Models;

use Attendance;
use DatabaseConnection\DatabaseConnection;

require_once('../DatabaseConnection/DatabaseConnection.php');
require_once('../Entity/Attendance.php');

/**
 * Class AttendanceModel
 * @package Models
 */
class AttendanceModel
{
    /**
     * @var \mysqli
     */
    private \mysqli $connection;

    /**
     * CourseModel constructor.
     */
    public function __construct()
    {
        $database = new DatabaseConnection();
        $this->connection = $database->database;
    }

    /**
     * @param $id
     * @return Attendance
     */
    public function createAttendance($id)
    {
        $attendance = new Attendance();
        $query = "SELECT * FROM attendances WHERE id=$id";
        $result = $this->connection->query($query);
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $attendance->setId($row['Id']);
                $attendance->setStudentId($row['Student_id']);
                $attendance->setCourseId($row['Course_id']);
                $attendance->setCourseAttendances($row['Course_attendances']);
                $attendance->setLaboratoryAttendances($row['Laboratory_attendances']);
                $attendance->setSeminarAttendances($row['Seminar_attendances']);
            }
            return $attendance;
        }
    }

    /**
     * @return mixed
     */
    public function getAttendances()
    {
        $attendances[] = new Attendance();
        $query = "SELECT id FROM attendances";
        $result = $this->connection->query($query);
        $ids = $result->fetch_all();
        $iterator = 0;
        foreach ($ids as $id) {
            $attendances[$iterator] = $this->createAttendance($id[0]);
            $iterator++;
        }

        return $attendances;
    }

    /**
     * @return bool
     */
    public function addAttendances()
    {

        if (isset($_POST['SubmitAttendance'])) {

            $student = $_POST['chooseStudent'];
            $course = $_POST['chooseCourse'];

            $query = "INSERT INTO attendances (Student_id, Course_id, Course_attendances, Laboratory_attendances, Seminar_attendances)
	            VALUES ($student, $course, 0, 0, 0)";

            mysqli_query($this->connection, $query);
            return true;
        }
        return false;

    }
    /**
     * @return bool
     */
    public function editAttendances()
    {
        if (isset($_POST['saveAttendances'])) {
            $attendanceId = $_POST['attendanceId'];
            $courseAttendances = $_POST['courseAttendance'];
            $laboratoryAttendances = $_POST['laboratoryAttendance'];
            $seminarAttendances = $_POST['seminarAttendance'];
            $query = "UPDATE attendances SET Course_attendances='$courseAttendances', 
                    Laboratory_attendances='$laboratoryAttendances', Seminar_attendances='$seminarAttendances' WHERE id = $attendanceId";

            mysqli_query($this->connection, $query);
            return true;
        }
        return false;
    }
}