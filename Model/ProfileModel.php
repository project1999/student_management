<?php

namespace Models;

use DatabaseConnection\DatabaseConnection;
require_once('../DatabaseConnection/DatabaseConnection.php');
require_once('../Model/StudentModel.php');
require_once('../Model/TeacherModel.php');
require_once('../Model/SecretaryModel.php');


/**
 * Class ProfileModel
 * @package Models
 */
class ProfileModel
{
    /**
     * @var \mysqli
     */
    private $connection;

    /**
     * @var StudentModel
     */
    private StudentModel $studentModel;

    /**
     * @var TeacherModel
     */
    private TeacherModel $teacherModel;

    /**
     * @var SecretaryModel
     */
    private SecretaryModel $secretaryModel;

    /**
     * ProfileModel constructor.
     */
    public function __construct()
    {
        $database = new DatabaseConnection();
        $this->connection = $database->database;
    }

    /**
     * @param $username
     * @return \Secretary|\Student|\Teacher
     */
    public function getDataUser($username)
    {
        switch ($_SESSION['user_role'])
        {
            case 'Secretary':
                $this->secretaryModel = new SecretaryModel();
                return $this->secretaryModel->createSecretary($username);
            case 'Teacher':
                $this->teacherModel = new TeacherModel();
                return $this->teacherModel->createTeacher($username);
            case 'Student':
                $this->studentModel = new StudentModel();
                return $this->studentModel->createStudent($username);
        }
    }
}