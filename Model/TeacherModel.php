<?php

namespace Models;

use DatabaseConnection\DatabaseConnection;
use Teacher;
require_once('../Entity/Teacher.php');
require_once('../DatabaseConnection/DatabaseConnection.php');
require_once('../Model/CourseModel.php');


/**
 * Class TeacherModel
 * @package Models
 */
class TeacherModel
{
    /**
     * @var \mysqli
     */
    private \mysqli $connection;

    /**
     * @var CourseModel
     */
    private $courseModel;

    /**
     * TeacherModel constructor.
     */
    public function __construct()
    {
        $database = new DatabaseConnection();
        $this->connection = $database->database;
        $this->courseModel = new CourseModel();
    }

    /**
     * @param $username
     * @return Teacher
     */
    public function createTeacher($username)
    {
        $query = "SELECT * FROM teachers WHERE username='$username'";
        $teacher = new Teacher();
        $result = $this->connection->query($query);
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $teacher->setId($row['Id']);
                $teacher->setPassword($row['Password']);
                $teacher->setFirstname($row['Firstname']);
                $teacher->setLastname($row['Lastname']);
                $teacher->setCnp($row['CNP']);
                $teacher->setPhone($row['Phone']);
                $teacher->setBirthday($row['Birthday']);
                $teacher->setAddress($row['Address']);
                $teacher->setUsername($row["Username"]);
                $teacher->setUserRole($row['User_role']);
                $teacher->setCourses($this->courseModel->getCoursesByTeacherId($teacher->getId()));
            }
            return $teacher;
        }
    }

    /**
     * @param $id
     * @return Teacher
     */
    public function createTeacherById($id)
    {
        $query = "SELECT * FROM teachers WHERE id=$id";
        $teacher = new Teacher();
        $result = $this->connection->query($query);
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $teacher->setId($id);
                $teacher->setPassword($row['Password']);
                $teacher->setFirstname($row['Firstname']);
                $teacher->setLastname($row['Lastname']);
                $teacher->setCnp($row['CNP']);
                $teacher->setPhone($row['Phone']);
                $teacher->setBirthday($row['Birthday']);
                $teacher->setAddress($row['Address']);
                $teacher->setUsername($row["Username"]);
                $teacher->setUserRole($row['User_role']);
                $teacher->setCourses($this->courseModel->getCoursesByTeacherId($id));
            }
            return $teacher;
        }
    }

    /**
     * @return mixed
     */
    public function getTeachers()
    {
        $teachers[] = new Teacher();
        $query = "SELECT username FROM teachers";
        $result = $this->connection->query($query);
        $usernames = $result->fetch_all();
        $iterator = 0;
        foreach ($usernames as $username) {
            $teachers[$iterator] = $this->createTeacher($username[0]);
            $iterator++;
        }
        return $teachers;
    }

    /**
     * @return bool
     */
    public function addTeacher()
    {
        if (isset($_POST['submit'])) {
            $firstname = $_POST['firstname'];
            $lastname = $_POST['lastname'];
            $username = $_POST['username'];
            $password = $_POST['password'];
            $user_role = 'Teacher';
            $cnp = $_POST['cnp'];
            $phone = $_POST['phone'];
            $birthday = $_POST['birthday'];
            $address = $_POST['address'];
            $query = "INSERT INTO teachers (Username, Password, User_role, Firstname, Lastname, CNP, Phone, Birthday, Address)
	            VALUES ('$username','$password','$user_role','$firstname','$lastname','$cnp','$phone','$birthday', '$address')";

            mysqli_query($this->connection, $query);
            return true;
        }
        return false;
    }

    /**
     * @return bool
     */
    public function editTeacher()
    {
        if (isset($_POST['save'])) {
            $firstname = $_POST['firstname'];
            $lastname = $_POST['lastname'];
            $username = $_POST['username'];
            $phone = $_POST['phone'];
            $query = "UPDATE teachers SET Username='$username', 
                    Firstname='$firstname', Lastname='$lastname', 
                    Phone='$phone' WHERE Username='$username'";

            mysqli_query($this->connection, $query);
            return true;
        }
        return false;
    }

    /**
     * @return bool
     */
    public function deleteTeacher()
    {
        if (isset($_POST['delete'])) {

            $username = $_POST['deleteUsername'];
            $query = "DELETE FROM teachers WHERE Username='$username'";

            mysqli_query($this->connection, $query);
            return true;
        }
        return false;
    }

    /**
     * @param $username
     * @return int
     */
    public function getTeacherId($username)
    {
        $query = "SELECT id FROM teachers WHERE username='$username'";
        $result = $this->connection->query($query);
        $teacherId = $result->fetch_all();
        return $teacherId[0];
    }
}