<?php

namespace Models;

use DatabaseConnection\DatabaseConnection;
use Models\LoginModel;

require_once('../DatabaseConnection/DatabaseConnection.php');
require_once('../Model/LoginModel.php');

/**
 * Class ChangePasswordModel
 * @package Models
 */
class ChangePasswordModel
{
    /**
     * @var \mysqli
     */
    private $connection;

    /**
     * @var LoginModel
     */
    private $loginModel;

    /**
     * ChangePasswordModel constructor.
     */
    public function __construct()
    {
        $database = new DatabaseConnection();
        $this->connection = $database->database;
        $this->loginModel = new LoginModel();
    }

    /**
     * @param $id
     * @param $newPassword
     */
    public function changePassword($id, $newPassword)
    {
        if ($this->verifyPassword($id)) {
            if($_SESSION['user_role'] == 'Student') {
                $query = "UPDATE secretaries
                          SET password = '$newPassword'
                          WHERE id = '$id'";
            } else if ($_SESSION['user_role'] == 'Teacher') {
                $query = "UPDATE teachers
                          SET password = '$newPassword'
                          WHERE id = '$id'";
            } else if ($_SESSION['user_role'] == 'Student') {
                $query = "UPDATE students
                          SET password = '$newPassword'
                          WHERE id = '$id'";
            }
            $this->connection->prepare($query)->execute();
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $id
     * @return bool
     */
    public function verifyPassword($id)
    {
        $id = mysqli_real_escape_string($this->connection, $id);

        if($_SESSION['user_role'] == 'Student') {
            $query = "SELECT * FROM secretaries WHERE id=$id";
        } else if ($_SESSION['user_role'] == 'Teacher') {
            $query = "SELECT * FROM teachers WHERE id=$id";
        } else if ($_SESSION['user_role'] == 'Student') {
            $query = "SELECT * FROM students WHERE id=$id";
        }

        $result = $this->connection->query($query);
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $currentPassword = $row['Password'];
            }
        }

        if ($currentPassword == $_POST['currentPassword']) {
            return true;
        } else {
            return false;
        }

    }
}
