<?php

namespace Models;

use DatabaseConnection\DatabaseConnection;
require_once('../DatabaseConnection/DatabaseConnection.php');

/**
 * Class LoginModel
 * @package Models
 */
class LoginModel
{
    /**
     * @var \mysqli
     */
    private $connection;

    /**
     * LoginModel constructor.
     */
    public function __construct()
    {
        $database = new DatabaseConnection();
        $this->connection = $database->database;
    }

    /**
     * @param $username
     * @return bool
     */
    public function login($username)
    {
        session_start();

        if (isset($_POST['submit'])) {
            $username = mysqli_real_escape_string($this->connection, $username);
            $query = $this->getQuery($username);
            $result = $this->connection->query($query);
            if ($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) {
                    $_SESSION['user_role'] = $row['User_role'];
                    $_SESSION['username'] = $row['Username'];
                    return true;
                }
            } else {
                return false;
            }
        }
    }

    /**
     * @param $username
     * @return string
     */
    public function getQuery($username)
    {
        if($_POST['chooseRole'] == 'Secretary') {
            $query = "SELECT * FROM secretaries WHERE username='$username'";
        } else if ($_POST['chooseRole'] == 'Teacher') {
            $query = "SELECT * FROM teachers WHERE username='$username'";
        } else if ($_POST['chooseRole'] == 'Student') {
            $query = "SELECT * FROM students WHERE username='$username'";
        }
        return $query;
    }
}
