<?php


namespace Models;

use Grade;
use DatabaseConnection\DatabaseConnection;

require_once('../DatabaseConnection/DatabaseConnection.php');
require_once('../Entity/Grade.php');

class GradeModel
{
    /**
     * @var \mysqli
     */
    private \mysqli $connection;

    /**
     * CourseModel constructor.
     */
    public function __construct()
    {
        $database = new DatabaseConnection();
        $this->connection = $database->database;
    }

    /**
     * @param $id
     * @return Grade
     */
    public function createGrade($id)
    {
        $grade = new Grade();
        $query = "SELECT * FROM grades WHERE id=$id";

        $result = $this->connection->query($query);
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $grade->setId($row['Id']);
                $grade->setStudentId($row['Student_id']);
                $grade->setCourseId($row['Course_id']);
                $grade->setCourseGrade($row['Course_grade']);
                $grade->setLaboratoryGrade($row['Laboratory_grade']);
                $grade->setSeminarGrade($row['Seminar_grade']);
            }
            return $grade;
        }
    }

    /**
     * @return mixed
     */
    public function getGrades()
    {
        $grades[] = new Grade();
        $query = "SELECT id FROM grades";
        $result = $this->connection->query($query);
        $ids = $result->fetch_all();
        $iterator = 0;
        foreach ($ids as $id) {
            $grades[$iterator] = $this->createGrade($id[0]);
            $iterator++;
        }

        return $grades;
    }

    /**
     * @return bool
     */
    public function addGrades()
    {

        if (isset($_POST['SubmitGrades'])) {

            $student = $_POST['chooseStudent'];
            $course = $_POST['chooseCourse'];

            $query = "INSERT INTO grades (Student_id, Course_id, Course_grade, Laboratory_grade, Seminar_grade)
	            VALUES ($student, $course, 0, 0, 0)";

            mysqli_query($this->connection, $query);
            return true;
        }
        return false;

    }

    /**
     * @return bool
     */
    public function editGrades()
    {
        if (isset($_POST['saveGrades'])) {
            $gradeId = $_POST['gradeId'];
            $courseGrade = $_POST['courseGrade'];
            $laboratoryGrade = $_POST['laboratoryGrade'];
            $seminarGrade = $_POST['seminarGrade'];
            $query = "UPDATE grades SET Course_grade='$courseGrade', 
                    Laboratory_grade='$laboratoryGrade', Seminar_grade='$seminarGrade' WHERE Id = $gradeId";

            mysqli_query($this->connection, $query);
            return true;
        }
        return false;
    }
}