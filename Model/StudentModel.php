<?php

namespace Models;

use DatabaseConnection\DatabaseConnection;
use Student;
require_once('../Entity/Student.php');
require_once('../DatabaseConnection/DatabaseConnection.php');

class StudentModel
{
    /**
     * @var \mysqli
     */
    private \mysqli $connection;

    /**
     * @var Student
     */
    private Student $student;

    /**
     * StudentModel constructor.
     */
    public function __construct()
    {
        $database = new DatabaseConnection();
        $this->connection = $database->database;
    }

    /**
     * @param $username
     * @return Student
     */
    public function createStudent($username)
    {
        $student = new Student();
        $query = "SELECT * FROM students WHERE username='$username'";
        $result = $this->connection->query($query);
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $student->setId($row['Id']);
                $student->setFirstname($row['Firstname']);
                $student->setLastname($row['Lastname']);
                $student->setCnp($row['CNP']);
                $student->setDadInitials($row['Dad_initials']);
                $student->setCredits($row['Credits']);
                $student->setPhone($row['Phone']);
                $student->setBirthday($row['Birthday']);
                $student->setAddress($row['Address']);
                $student->setUsername($row["Username"]);
                $student->setUserRole($row['User_role']);
            }
            return $student;
        }
    }

    /**
     * @param $id
     * @return Student
     */
    public function createStudentById($id)
    {
        $query = "SELECT * FROM Students WHERE id=$id";
        $student = new Student();
        $result = $this->connection->query($query);
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $student->setId($row['Id']);
                $student->setFirstname($row['Firstname']);
                $student->setLastname($row['Lastname']);
                $student->setCnp($row['CNP']);
                $student->setDadInitials($row['Dad_initials']);
                $student->setCredits($row['Credits']);
                $student->setPhone($row['Phone']);
                $student->setBirthday($row['Birthday']);
                $student->setAddress($row['Address']);
                $student->setUsername($row["Username"]);
                $student->setUserRole($row['User_role']);
            }
            return $student;
        }
    }

    /**
     * @return mixed
     */
    public function getStudents()
    {
        $students[] = new Student();
        $query = "SELECT username FROM students";
        $result = $this->connection->query($query);
        $usernames = $result->fetch_all();
        $iterator = 0;
        foreach ($usernames as $username) {
            $students[$iterator] = $this->createStudent($username[0]);
            $iterator++;
        }
        return $students;
    }

    /**
     * @return bool
     */
    public function addStudent()
    {
        if (isset($_POST['submit'])) {
            $firstname = $_POST['firstname'];
            $lastname = $_POST['lastname'];
            $username = $_POST['username'];
            $password = $_POST['password'];
            $user_role = 'Student';
            $cnp = $_POST['cnp'];
            $phone = $_POST['phone'];
            $birthday = $_POST['birthday'];
            $address = $_POST['address'];
            $credits = $_POST['credits'];
            $dad_initials = $_POST['dad_initials'];
            $query = "INSERT INTO students (Username, Password, User_role, Firstname, Lastname, CNP, Phone, Birthday, Address, Credits, Dad_initials)
	            VALUES ('$username','$password','$user_role','$firstname','$lastname','$cnp','$phone','$birthday', '$address', $credits, '$dad_initials')";

            mysqli_query($this->connection, $query);
            return true;
        }
        return false;
    }

    /**
     * @return bool
     */
    public function editStudent()
    {
        if (isset($_POST['save'])) {
            $firstname = $_POST['firstname'];
            $lastname = $_POST['lastname'];
            $username = $_POST['username'];
            $phone = $_POST['phone'];
            $credits = $_POST['credits'];
            $query = "UPDATE students SET Firstname='$firstname', Lastname='$lastname', 
                    Phone='$phone', Credits=$credits WHERE Username='$username'";

            mysqli_query($this->connection, $query);
            return true;
        }
        return false;
    }

    /**
     * @return bool
     */
    public function deleteStudent()
    {
        if (isset($_POST['delete'])) {
            $username = $_POST['deleteUsername'];
            $query = "DELETE FROM students WHERE Username='$username'";
            mysqli_query($this->connection, $query);
            return true;
        }
        return false;
    }
}