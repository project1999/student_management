<?php

/**
 * Class Course
 */
class Course
{
    /**
     * @var INT
     */
    private $id;

    /**
     * @var STRING
     */
    private $name;

    /**
     * @var INT
     */
    private $teacherId;

    /**
     * @var boolean
     */
    private $laboratory;

    /**
     * @var boolean
     */
    private $seminar;

    /**
     * @var INT
     */
    private $credits;

    /**
     * @var STRING
     */
    private $material;

    /**
     * Course constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return INT
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param INT $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return STRING
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param STRING $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return INT
     */
    public function getTeacherId(): int
    {
        return $this->teacherId;
    }

    /**
     * @param INT $teacherId
     */
    public function setTeacherId(int $teacherId): void
    {
        $this->teacherId = $teacherId;
    }

    /**
     * @return bool
     */
    public function hasLaboratory(): bool
    {
        return $this->laboratory;
    }

    /**
     * @param bool $laboratory
     */
    public function setLaboratory(bool $laboratory): void
    {
        $this->laboratory = $laboratory;
    }

    /**
     * @return bool
     */
    public function hasSeminar(): bool
    {
        return $this->seminar;
    }

    /**
     * @param bool $seminar
     */
    public function setSeminar(bool $seminar): void
    {
        $this->seminar = $seminar;
    }

    /**
     * @return INT
     */
    public function getCredits(): int
    {
        return $this->credits;
    }

    /**
     * @param INT $credits
     */
    public function setCredits(int $credits): void
    {
        $this->credits = $credits;
    }

    /**
     * @return STRING
     */
    public function getMaterial(): string
    {
        return $this->material;
    }

    /**
     * @param STRING $material
     */
    public function setMaterial(string $material): void
    {
        $this->material = $material;
    }

}