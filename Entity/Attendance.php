<?php

/**
 * Class Attendance
 */
class Attendance
{
    /**
     * @var INT
     */
    private $id;

    /**
     * @var INT
     */
    private $studentId;

    /**
     * @var INT
     */
    private $courseId;

    /**
     * @var INT
     */
    private $courseAttendances;

    /**
     * @var INT
     */
    private $laboratoryAttendances;

    /**
     * @var INT
     */
    private $seminarAttendances;

    /**
     * Attendance constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return INT
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param INT $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return INT
     */
    public function getStudentId(): int
    {
        return $this->studentId;
    }

    /**
     * @param INT $studentId
     */
    public function setStudentId(int $studentId): void
    {
        $this->studentId = $studentId;
    }

    /**
     * @return INT
     */
    public function getCourseId(): int
    {
        return $this->courseId;
    }

    /**
     * @param INT $courseId
     */
    public function setCourseId(int $courseId): void
    {
        $this->courseId = $courseId;
    }

    /**
     * @return INT
     */
    public function getCourseAttendances(): int
    {
        return $this->courseAttendances;
    }

    /**
     * @param INT $courseAttendances
     */
    public function setCourseAttendances(int $courseAttendances): void
    {
        $this->courseAttendances = $courseAttendances;
    }

    /**
     * @return INT
     */
    public function getLaboratoryAttendances(): int
    {
        return $this->laboratoryAttendances;
    }

    /**
     * @param INT $laboratoryAttendances
     */
    public function setLaboratoryAttendances(int $laboratoryAttendances): void
    {
        $this->laboratoryAttendances = $laboratoryAttendances;
    }

    /**
     * @return INT
     */
    public function getSeminarAttendances(): int
    {
        return $this->seminarAttendances;
    }

    /**
     * @param INT $seminarAttendances
     */
    public function setSeminarAttendances(int $seminarAttendances): void
    {
        $this->seminarAttendances = $seminarAttendances;
    }


}