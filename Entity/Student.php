<?php

/**
 * Class Student
 */
class Student
{
    /**
     * @var INT
     */
    private $id;

    /**
     * @var STRING
     */
    private $firstname;

    /**
     * @var STRING
     */
    private $lastname;

    /**
     * @var STRING
     */
    private $username;

    /**
     * @var STRING
     */
    private $password;

    /**
     * @var STRING
     */
    private $cnp;

    /**
     * @var STRING
     */
    private $dad_initials;

    /**
     * @var INT
     */
    private $credits;

    /**
     * @var STRING
     */
    private $phone;

    /**
     * @var STRING
     */
    private $birthday;

    /**
     * @var STRING
     */
    private $address;

    /**
     * @var STRING
     */
    private $user_role;

    /**
     * Student constructor.
     */
    public function __construct()
    {

    }

    /**
     * @return INT
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param INT $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return STRING
     */
    public function getFirstname(): string
    {
        return $this->firstname;
    }

    /**
     * @param STRING $firstname
     */
    public function setFirstname(string $firstname): void
    {
        $this->firstname = $firstname;
    }

    /**
     * @return STRING
     */
    public function getLastname(): string
    {
        return $this->lastname;
    }

    /**
     * @param STRING $lastname
     */
    public function setLastname(string $lastname): void
    {
        $this->lastname = $lastname;
    }

    /**
     * @return STRING
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param STRING $username
     */
    public function setUsername(string $username): void
    {
        $this->username = $username;
    }

    /**
     * @return STRING
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param STRING $password
     */
    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    /**
     * @return STRING
     */
    public function getCnp(): string
    {
        return $this->cnp;
    }

    /**
     * @param STRING $cnp
     */
    public function setCnp(string $cnp): void
    {
        $this->cnp = $cnp;
    }

    /**
     * @return STRING
     */
    public function getDadInitials(): string
    {
        return $this->dad_initials;
    }

    /**
     * @param STRING $dad_initials
     */
    public function setDadInitials(string $dad_initials): void
    {
        $this->dad_initials = $dad_initials;
    }

    /**
     * @return INT
     */
    public function getCredits(): int
    {
        return $this->credits;
    }

    /**
     * @param INT $credits
     */
    public function setCredits(int $credits): void
    {
        $this->credits = $credits;
    }

    /**
     * @return STRING
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @param STRING $phone
     */
    public function setPhone(string $phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return STRING
     */
    public function getBirthday(): string
    {
        return $this->birthday;
    }

    /**
     * @param STRING $birthday
     */
    public function setBirthday(string $birthday): void
    {
        $this->birthday = $birthday;
    }

    /**
     * @return STRING
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @param STRING $address
     */
    public function setAddress(string $address): void
    {
        $this->address = $address;
    }

    /**
     * @return STRING
     */
    public function getUserRole(): string
    {
        return $this->user_role;
    }

    /**
     * @param STRING $user_role
     */
    public function setUserRole(string $user_role): void
    {
        $this->user_role = $user_role;
    }
}