<?php

/**
 * Class Grade
 */
class Grade
{
    /**
     * @var INT
     */
    private $id;

    /**
     * @var INT
     */
    private $studentId;

    /**
     * @var INT
     */
    private $courseId;

    /**
     * @var INT
     */
    private $courseGrade;

    /**
     * @var INT
     */
    private $laboratoryGrade;

    /**
     * @var INT
     */
    private $seminarGrade;

    /**
     * Grade constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return INT
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param INT $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return INT
     */
    public function getStudentId(): int
    {
        return $this->studentId;
    }

    /**
     * @param INT $studentId
     */
    public function setStudentId(int $studentId): void
    {
        $this->studentId = $studentId;
    }

    /**
     * @return INT
     */
    public function getCourseId(): int
    {
        return $this->courseId;
    }

    /**
     * @param INT $courseId
     */
    public function setCourseId(int $courseId): void
    {
        $this->courseId = $courseId;
    }

    /**
     * @return INT
     */
    public function getCourseGrade(): int
    {
        return $this->courseGrade;
    }

    /**
     * @param INT $courseGrade
     */
    public function setCourseGrade(int $courseGrade): void
    {
        $this->courseGrade = $courseGrade;
    }

    /**
     * @return INT
     */
    public function getLaboratoryGrade(): int
    {
        return $this->laboratoryGrade;
    }

    /**
     * @param INT $laboratoryGrade
     */
    public function setLaboratoryGrade(int $laboratoryGrade): void
    {
        $this->laboratoryGrade = $laboratoryGrade;
    }

    /**
     * @return INT
     */
    public function getSeminarGrade(): int
    {
        return $this->seminarGrade;
    }

    /**
     * @param INT $seminarGrade
     */
    public function setSeminarGrade(int $seminarGrade): void
    {
        $this->seminarGrade = $seminarGrade;
    }
}