<?php

namespace DatabaseConnection;

use mysqli;

/**
 * Class DatabaseConnection
 * @package DatabaseConnection
 */
class DatabaseConnection
{
    /**
     * @var mysqli
     */
    public mysqli $database;


    /**
     * DatabaseConnection constructor.
     */
    public function __construct()
    {
        $this->database = new mysqli ("localhost", "root", "", "studentmanagement");

        if ($this->database->connect_error) {
            die("Connection failed: " . $this->database->connect_error);
        }
    }
}