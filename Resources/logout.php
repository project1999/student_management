<?php
/**
 *  A special php file for logout button
 */
    session_start();
    session_unset();
    session_destroy();
    header("Location: ../View/Login.phtml");
?>