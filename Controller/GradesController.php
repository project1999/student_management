<?php

namespace Controllers;

use Models\GradeModel;

require_once('../Model/GradeModel.php');

/**
 * Class GradesController
 * @package Controllers
 */
class GradesController
{
    /**
     * @var GradeModel
     */
    private GradeModel $gradeModel;

    /**
     * StudentsController constructor.
     */
    public function __construct()
    {
        $this->gradeModel = new GradeModel();
    }

    public function invoke()
    {

        if ($this->gradeModel->editGrades()) {
            header("Location: Grades.phtml");
        }
        if ($this->gradeModel->addGrades()) {
            header("Location: Grades.phtml");
        }
        return $this->gradeModel->getGrades();
    }
}