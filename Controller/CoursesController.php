<?php

namespace Controllers;

use Models\CourseModel;
require_once('../Model/CourseModel.php');

class CoursesController
{
    /**
     * @var CourseModel
     */
    private CourseModel $courseModel;

    /**
     * TeachersController constructor.
     */
    public function __construct()
    {
        $this->courseModel = new CourseModel();
    }

    public function invoke()
    {
        if ($this->courseModel->addCourse()) {
            header("Location: Courses.phtml");
        }

        if ($this->courseModel->editCourse()) {
            header("Location: Courses.phtml");
        }

        if ($this->courseModel->deleteCourse()) {
            header("Location: Courses.phtml");
        }
        return $this->courseModel->getCourses();
    }
}