<?php

namespace Controllers;

use Models\ChangePasswordModel;
require_once('../Model/ChangePasswordModel.php');

/**
 * Class ChangePasswordController
 * @package Controllers
 */
class ChangePasswordController
{
    /**
     * @var ChangePasswordModel
     */
    public ChangePasswordModel $changePasswordModel;

    /**
     * ChangePasswordController constructor.
     */
    public function __construct()
    {
        session_start();
        $this->changePasswordModel = new ChangePasswordModel();
    }

    public function invoke()
    {
        if (($this->changePasswordModel->changePassword($_SESSION['id'], $_POST['newPassword']) == false) || ($_POST['newPassword'] != $_POST['confirmPassword'])) {
            header("Location: IncorrectChangePassword.phtml");
        } else {
            $this->changePasswordModel->changePassword($_SESSION['id'], $_POST['newPassword']);
            header("Location: Home.phtml");
        }
    }
}