<?php

namespace Controllers;

use Models\ProfileModel;
require_once('../Model/ProfileModel.php');

/**
 * Class ProfileController
 * @package Controllers
 */
class ProfileController
{
    public ProfileModel $profileModel;

    /**
     * ProfileController constructor.
     */
    public function __construct()
    {
        $this->profileModel = new ProfileModel();
    }

    public function invoke()
    {
        if($this->profileModel->getDataUser($_SESSION['username']) == false) {
            header("Location: IncorrectChangePassword.phtml");
        }

        return $this->profileModel->getDataUser($_SESSION['username']);
    }
}