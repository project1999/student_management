<?php

namespace Controllers;

use Models\LoginModel;
require_once('../Model/LoginModel.php');

/**
 * Class LoginController
 * @package Controllers
 */
class LoginController
{
    public LoginModel $loginModel;

    /**
     * LoginController constructor.
     */
    public function __construct()
    {
        $this->loginModel = new LoginModel();
    }

    public function invoke()
    {
        $isLogged = $this->loginModel->login($_POST['username']);
        if ($isLogged) {
            header("Location: Home.phtml");
        } else {
            header("Location: IncorrectLogin.phtml");
        }
    }
}