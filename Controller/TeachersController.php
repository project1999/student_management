<?php

namespace Controllers;

use Models\TeacherModel;
require_once('../Model/TeacherModel.php');

/**
 * Class TeachersController
 * @package Controllers
 */
class TeachersController
{
    /**
     * @var TeacherModel
     */
    private TeacherModel $teacherModel;

    /**
     * TeachersController constructor.
     */
    public function __construct()
    {
        $this->teacherModel = new TeacherModel();
    }

    public function invoke()
    {
        if ($this->teacherModel->addTeacher()) {
            header("Location: Teachers.phtml");
        }

        if ($this->teacherModel->editTeacher()) {
            header("Location: Teachers.phtml");
        }

        if ($this->teacherModel->deleteTeacher()) {
            header("Location: Teachers.phtml");
        }
        return $this->teacherModel->getTeachers();
    }

}