<?php

namespace Controllers;

use Models\StudentModel;
require_once('../Model/StudentModel.php');

/**
 * Class StudentsController
 * @package Controllers
 */
class StudentsController
{
    /**
     * @var StudentModel
     */
    private StudentModel $studentModel;

    /**
     * StudentsController constructor.
     */
    public function __construct()
    {
        $this->studentModel = new StudentModel();
    }

    public function invoke()
    {
        if ($this->studentModel->addStudent()) {
            header("Location: Students.phtml");
        }

        if ($this->studentModel->editStudent()) {
            header("Location: Students.phtml");
        }

        if ($this->studentModel->deleteStudent()) {
            header("Location: Students.phtml");
        }

        return $this->studentModel->getStudents();
    }
}