<?php

namespace Controllers;

use Models\AttendanceModel;

require_once('../Model/AttendanceModel.php');

/**
 * Class AttendancesController
 * @package Controllers
 */
class AttendancesController
{
    /**
     * @var AttendanceModel
     */
    private AttendanceModel $attendanceModel;

    /**
     * StudentsController constructor.
     */
    public function __construct()
    {
        $this->attendanceModel = new AttendanceModel();
    }

    public function invoke()
    {

        if ($this->attendanceModel->editAttendances()) {
            header("Location: Attendances.phtml");
        }
        if ($this->attendanceModel->addAttendances()) {
            header("Location: Attendances.phtml");
        }
        return $this->attendanceModel->getAttendances();
    }
}